create table users(
    id varchar2(255) primary key,
    user_name varchar(255) not null,
    password varchar(255) not null,
    role varchar2(255) not null,
    status number(2) not null,
    created_at date,
    created_by varchar2(255)
);

create table roles(
    id varchar2(255) primary key,
    role_name varchar2(255) not null,
    description varchar2(500),
    status number(2)
);

create table category(
    id varchar2(255) primary key,
    parent_id varchar2(255),
    name_en varchar2(1000),
    name_lc varchar2(1000),
    desc_en varchar2(2000),
    desc_lc varchar2(2000),
    image_url_en varchar2(1000),
    image_url_lc varchar2(1000),
    status number(2),
    created_at date,
    updated_at date,
    created_by varchar2(255),
    updated_by varchar2(255)
);

create table news(
    id varchar2(255) primary key,
    category_id varchar2(255),
    title_en varchar2(1000),
    title_lc varchar2(1000),
    summary_en varchar2(4000),
    summary_lc varchar2(4000),
    content_en clob,
    content_lc clob,
    status number(2),
    created_at date,
    created_by varchar2(255),
    updated_at date,
    updated_by varchar2(255),
    approved_at date,
    approved_by varchar2(255),
    publish_at date,
    expired_at date
);
COMMENT ON COLUMN news.status IS '0 - invalid, 1 - draft, 2 - submitted, 3 - approved, 4 - published, 5 - expired';

create table asset(
    id varchar2(255) primary key,
    parent_id varchar2(255),
    type number(2),
    name varchar2(255),
    link varchar2(1000),
    status number(2),
    created_at date,
    created_by varchar2(255)
);
COMMENT ON COLUMN asset.type IS '0 - file, 1 - folder';

INSERT INTO NEWS.ROLES (ID, ROLE_NAME, DESCRIPTION, STATUS) VALUES ('C6C4C1CC02100FF2E05011AC02000093', 'ADMIN', null, 1);
INSERT INTO NEWS.ROLES (ID, ROLE_NAME, DESCRIPTION, STATUS) VALUES ('C6C4C1CC02110FF2E05011AC02000093', 'EDITOR', null, 1);

-- 123456a@
INSERT INTO NEWS.USERS (ID, USER_NAME, PASSWORD, ROLE, STATUS, CREATED_AT, CREATED_BY) VALUES ('C6C5D221429C27BDE05011AC0200013B', 'news_admin', '$2a$10$grk.xVMmDFcoI9N28wFBcuRnAAr74kI6ddbDoeXiPVf.Pwk4yFBMK', 'C6C4C1CC02100FF2E05011AC02000093', 1, TO_DATE('2021-07-10 13:25:25', 'YYYY-MM-DD HH24:MI:SS'), null);