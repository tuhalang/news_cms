FROM tomcat:9.0
MAINTAINER tuhalang
COPY target/NEWS_CMS.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh", "run"]