package com.viettel.news_cms.repo;

import com.viettel.news_cms.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, String>, CommonRepo {

    User findByUserName(String userName);
    boolean existsByUserName(String userName);


}
