package com.viettel.news_cms.repo;

import com.viettel.news_cms.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepo extends JpaRepository<Role, String> {

    Role findByRoleNameAndStatus(String roleName, Integer status);
}
