package com.viettel.news_cms.repo;

import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface CommonRepo {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(value= "select SYSDATE from dual", nativeQuery=true)
    Date currentDate();
}
