package com.viettel.news_cms.dto;

import com.viettel.news_cms.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto {

    private String errorCode = Constant.ERROR_CODE_NOK;
    private String message;
    private LinkedHashMap<String, Object> data;

    public void pushData(String key, Object value){
        if(this.data == null){
            this.data = new LinkedHashMap<>();
        }
        this.data.put(key, value);
    }
}
