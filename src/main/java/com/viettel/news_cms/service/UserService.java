package com.viettel.news_cms.service;

import com.viettel.news_cms.dto.AccountDto;
import com.viettel.news_cms.dto.ResponseDto;

public interface UserService {

    ResponseDto signIn(AccountDto accountDto);
    ResponseDto signUp(AccountDto accountDto);
}
