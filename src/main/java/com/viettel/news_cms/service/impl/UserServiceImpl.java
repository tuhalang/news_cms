package com.viettel.news_cms.service.impl;

import com.viettel.news_cms.domain.User;
import com.viettel.news_cms.dto.AccountDto;
import com.viettel.news_cms.dto.ResponseDto;
import com.viettel.news_cms.repo.RoleRepo;
import com.viettel.news_cms.repo.UserRepo;
import com.viettel.news_cms.security.JwtTokenProvider;
import com.viettel.news_cms.service.UserService;
import com.viettel.news_cms.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@RequiredArgsConstructor
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;

    @Override
    public ResponseDto signIn(AccountDto accountDTO) {

        ResponseDto responseDto = ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_NOK)
                .build();

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            accountDTO.getUsername(),
                            accountDTO.getPassword(),
                            Collections.emptyList()
                    )
            );
            SimpleGrantedAuthority sga = (SimpleGrantedAuthority) authentication.getAuthorities().iterator().next();
            String jwt = jwtTokenProvider.generateUserToken(accountDTO.getUsername(), sga.getAuthority());

            responseDto.setErrorCode(Constant.ERROR_CODE_OK);
            responseDto.setMessage(Constant.MSG_SIGN_IN_OK);
            responseDto.pushData("token", jwt);
            responseDto.pushData("roles", sga.getAuthority());
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setMessage(e.getMessage());
        }

        return responseDto;
    }

    @Override
    public ResponseDto signUp(AccountDto accountDto) {
        ResponseDto responseDto = ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_NOK)
                .build();

        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User userActive = (User) authentication.getPrincipal();

            if(!userRepo.existsByUserName(accountDto.getUsername())){
                User user = User.builder()
                        .id(userRepo.getID())
                        .userName(accountDto.getUsername())
                        .password(passwordEncoder.encode(accountDto.getPassword()))
                        .role(roleRepo.findByRoleNameAndStatus(accountDto.getRoleName(), Constant.STATUS_ACTIVE).getId())
                        .status(Constant.STATUS_ACTIVE)
                        .createdBy(userActive.getUserName())
                        .createdAt(userRepo.currentDate())
                        .build();
                userRepo.save(user);

                responseDto.setErrorCode(Constant.ERROR_CODE_OK);
                responseDto.setMessage(Constant.MSG_SIGN_UP_OK);
            }else{
                responseDto.setMessage(Constant.MSG_SIGN_UP_NOK_USER_EXISTS);
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setMessage(e.getMessage());
        }

        return responseDto;
    }
}
