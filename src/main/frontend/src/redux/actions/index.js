import {GET_MENUS, LOGIN} from "./action_types";

export const getMenus = () => {
    return {
        type: GET_MENUS
    }
}

export const login = ({email, password}) => {
    return {
        type: LOGIN,
        email,
        password,
    }
}
