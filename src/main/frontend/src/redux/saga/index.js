import {call, put, takeLatest} from 'redux-saga/effects'
import {GET_MENUS, GET_MENUS_SUCCESS, LOGIN} from "../actions/action_types";

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* login(action) {
    console.log("======== login ========")
    try {
        // // const res = await apiUser.login({ ...values, namespace: "uscel" })
        // const {
        //     type,
        //     token,
        //     fullname,
        //     avatar,
        //     merchant_type,
        //     refreshToken,
        //     agree_policy,
        // } = res
        // if (type === "Error") {
        //     message.error("Login failed")
        // } else {
        //     localStorage.setItem("accessToken", token)
        //     localStorage.setItem("refreshToken", refreshToken)
        //     localStorage.setItem("userName", fullname)
        //     localStorage.setItem("avatar", avatar)
        //     localStorage.setItem("merchant_type", merchant_type)
        //     localStorage.setItem("agree_policy", agree_policy || false)
        //     delete res.token
        //     onChangeUserInfo(res)
        //     setLoading(false)
        //     if (merchant_type === "seller") {
        //         history.push("/orders")
        //     } else if (merchant_type === "supplier") {
        //         history.push("/pieces")
        //     } else {
        //         history.push("/")
        //     }
        // }
        localStorage.setItem("accessToken", "token")
    } catch (e) {
        console.log(e)
    }
}

function* getMenus(action) {
    try {
        const menus = localStorage.getItem("menus")
        let data = [
            {
                code: "CUSTOMERS",
                title: "Customers"
            },
            {
                code: "DASHBOARD",
                title: "Dashboard"
            }
        ]
        if (menus) {
            data = JSON.parse(menus)
        } else {
            // data = await apiUser.getMenus()
            localStorage.setItem("menus", JSON.stringify(data))
        }
        if (data.length > 0) {
            yield put({
                type: GET_MENUS_SUCCESS,
                payload: data,
            })
        }
    } catch (e) {
        console.log(e)
    }
}

/*
  Alternatively you may use takeLatest.

  Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
function* rootSaga() {
    yield takeLatest(LOGIN, login);
    yield takeLatest(GET_MENUS, getMenus);
}

export default rootSaga;
