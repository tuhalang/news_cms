import ThemeReducer from "./ThemeReducer"
import user from "./user"
import {combineReducers} from "redux"

const rootReducer = combineReducers({
    user,
    ThemeReducer,
})

export default rootReducer
