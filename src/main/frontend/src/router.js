import React, {Suspense, useEffect, Fragment} from "react"
import {
    Route,
    Switch,
    Redirect,
    Link, HashRouter,
} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux"
import {Result} from "antd"
import Layout from "./components/layout/Layout";
import {getMenus} from "./redux/actions";
import SuspenseComponent from "./components/suspense/SuspenseComponent";
import SpinnerLoadMenu from "./components/suspense/SpinnerLoadMenu";
import Customers from "./pages/Customers";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Common/Login";
// import ModalEvent from "components/Modal/ModalEvent"
// import Verify from "pages/auth/Verify"
// import ForgotPassword from "pages/auth/ForgotPassword"
// import ChangePassword from "pages/auth/ChangePassword"
//
// import RegisterUsel from "pages/auth/RegisterUsel"
// import Layout from "components/Layout/Layout"

// const User = React.lazy(() => import("pages/User"));

function PrivateRoute({children, ...rest}) {
    const checkToken = localStorage.getItem("accessToken")
    return (
        <Route
            {...rest}
            render={() => (checkToken ? children : <Redirect to="/login"/>)}
        />
    )
}

const ProtectedPage = () => {
    const dispatch = useDispatch()
    const {menus} = useSelector(state => state.user)

    function WaitingComponent(Component, code) {
        if (menus.find((i) => i.code === code)) {
            return (props) => (
                <Suspense fallback={<SuspenseComponent/>}>
                    <Component {...props} />
                </Suspense>
            )
        } else {
            return NotLoadAuthorization
        }
    }

    useEffect(() => {
        dispatch(getMenus())
        // // if (merchant_type === "customer_ship" || merchant_type === "fulfillment") {
        // //   dispatch(getServices())
        // //   dispatch(getStatus())
        // //   dispatch(getLineShips())
        // // }
        // if (merchant_type === KEY.SUPPLIER) {
        //     dispatch(getStatus())
        //     dispatch(getServices())
        // }
        // if (merchant_type === KEY.SELLER) {
        //     dispatch(getBalance())
        // }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Fragment>
            {/* <ModalNewyear /> */}
            {/* <ModalEvent /> */}
            {/*{menus.length > 0 ? (*/}
            <Layout>
                <Switch>
                    <Route
                        path="/customers"
                        exact
                        component={WaitingComponent(Customers, "CUSTOMERS")}
                    />
                    <Route
                        path="/"
                        exact
                        component={WaitingComponent(Dashboard, "DASHBOARD")}
                    />
                    <Route component={NotLoadAuthorization}/>
                </Switch>
            </Layout>
            {/*) : (*/}
            {/*    // <SpinnerLoadMenu />*/}
            {/*    <div/>*/}
            {/*)}*/}
        </Fragment>
    )
}

const Routes = () => {
    return (
        <HashRouter>
            <Switch>
                <Route exact path="/login" component={Login}/>
                {/*<Route exact path="/verify" component={Verify} />*/}
                {/*<Route exact path="/register" component={RegisterUsel} />*/}
                {/*<Route exact path="/forgot-password" component={ForgotPassword} />*/}
                {/*<Route exact path="/reset-password" component={ChangePassword} />*/}
                <PrivateRoute path="/">
                    <ProtectedPage/>
                </PrivateRoute>
            </Switch>
        </HashRouter>
    )
}

export default Routes

const NotLoadAuthorization = () => {
    const themeReducer = useSelector(state => state.ThemeReducer)

    return (
        <Result
            status="404"
            title={<span className={themeReducer.result}>404</span>}
            subTitle={<span className={themeReducer.result}>Trang không tồn tại</span>}
            extra={<Link to="/">Trở về trang chủ</Link>}
        />
    )
}
