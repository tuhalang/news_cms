import React from 'react'

import {useSelector} from "react-redux";
import TinyEditor from "../components/editor/TinyEditor";
import {Button, Table} from "antd";
import Filter from "../components/filter/Filter";
import {paramsUrl} from "../components/function";

const columns = [
    {
        title: 'No',
        width: 100,
        dataIndex: 'name',
        key: 'name',
        fixed: 'left',
    },
    {
        title: 'Age',
        width: 100,
        dataIndex: 'age',
        key: 'age',
        fixed: 'left',
    },
    {
        title: 'Column 1',
        dataIndex: 'address',
        key: '1',
        width: 150,
    },
    {
        title: 'Column 2',
        dataIndex: 'address',
        key: '2',
        width: 150,
    },
    {
        title: 'Column 3',
        dataIndex: 'address',
        key: '3',
        width: 150,
    },
    {
        title: 'Column 4',
        dataIndex: 'address',
        key: '4',
        width: 150,
    },
    {
        title: 'Column 5',
        dataIndex: 'address',
        key: '5',
        width: 150,
    },
    {
        title: 'Column 6',
        dataIndex: 'address',
        key: '6',
        width: 150,
    },
    {
        title: 'Column 7',
        dataIndex: 'address',
        key: '7',
        width: 150,
    },
    {title: 'Column 8', dataIndex: 'address', key: '8'},
    {
        title: 'Action',
        key: 'operation',
        fixed: 'right',
        width: 100,
        render: () => <a>action</a>,
    },
];

const Customers = () => {

    const themeReducer = useSelector(state => state.ThemeReducer)
    const [visible, setVisible] = React.useState(false)

    const __pagination = React.useRef({
        pageNumber: 1,
        pageSize: 10,
        totalItems: 0,
        ...paramsUrl.get(),
    })

    function onFilter(params) {
        let newParams = {
            ...params,
            is_hold: undefined,
            paid: undefined,
        }
        __pagination.current = {
            ...__pagination.current,
            pageNumber: 1,
            ...newParams,
        }
    }

    const data = [];
    for (let i = 0; i < 100; i++) {
        data.push({
            key: i,
            name: `Edrward ${i}`,
            age: 32,
            address: `London Park no. ${i}`,
        });
    }

    return (
        <div>
            <h2 className={`page-header ${themeReducer.word}`}>
                customers
            </h2>
            <Button onClick={() => setVisible(true)}>Open Editor</Button>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <Filter filter={__pagination.current} onFilter={onFilter}/>
                        <div className="card__body">
                            <Table columns={columns} dataSource={data} scroll={{x: 1500}}/>
                        </div>
                    </div>
                </div>
            </div>
            <TinyEditor visible={visible} onClose={() => setVisible(false)}/>
        </div>
    )
}

export default Customers
