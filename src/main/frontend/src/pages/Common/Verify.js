// import getFactory from "api"
// import { Spin } from "antd"
// import queryString from "query-string"
// import { useEffect, useState } from "react"
// import { useHistory } from "react-router"
// const api = getFactory("user")
// export default function Verify() {
//   const history = useHistory()
//   const { token } = queryString.parse(window.location.search)
//   const [loading, setLoading] = useState(false)
//   async function verify() {
//     try {
//       setLoading(true)
//       await api.verifyAccount({ token })
//       history.push("/signin")
//     } catch (error) {
//       console.log(error)
//     } finally {
//       setLoading(true)
//     }
//   }
//
//   useEffect(() => {
//     verify()
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [])
//
//   return (
//     <Spin spinning={loading}>
//       <div className="text-center p-4 h1">Loading</div>
//     </Spin>
//   )
// }
