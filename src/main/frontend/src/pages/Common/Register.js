// import React, { useState } from "react"
// import { Form, Input, Button, Row, Col, message, notification } from "antd"
// import { useHistory, Link } from "react-router-dom"
// import getFactory from "api"
// import wordwideBg from "images/wordwide.svg"
// import "./index.scss"
//
// const apiUser = getFactory("user")
// const SignUp = () => {
//   const history = useHistory()
//
//   const [loading, setLoading] = useState(false)
//
//   async function handleSubmit(values) {
//     try {
//       if (values["password"] !== values["confirm_password"]) {
//         message.error("The two passwords that you entered do not match!")
//         setLoading(false)
//         return false
//       }
//       setLoading(true)
//       values.referrer = values.referrer ? values.referrer.trim() : undefined
//       const res = await apiUser.signUp({ ...values, namespace: "vcs" })
//       const { type } = res
//       if (type === "Error") {
//         message.error("Register false, Please try agian!.")
//         setLoading(false)
//         return false
//       } else {
//         notification.success({
//           message: "Notification",
//           description: res.message,
//         })
//         setLoading(false)
//         localStorage.setItem("r", "s")
//         localStorage.setItem("n", values["fullname"])
//         localStorage.setItem("m", values["email"])
//         history.push(`/signin?lang=vi&utm_medium=vcsregister`)
//       }
//     } catch (e) {
//       setLoading(false)
//     }
//   }
//
//   function onFinishFailed(params) {}
//
//   return (
//     <Row className="page-container vcs-page">
//       <Col xs={24} sm={12} md={12} lg={10}>
//         <div className="logo">
//           <div className="logo-vcs" />
//           <div className="logo-text"> EXPRESS </div>
//         </div>
//         <div className="page_login">
//           <div className="login_form is-floating-label">
//             <div className="text4xl f-500 mb-4">Register</div>
//             <Form
//               layout="vertical"
//               name="basic"
//               initialValues={{ remember: true }}
//               onFinish={handleSubmit}
//               onFinishFailed={onFinishFailed}
//             >
//               <Form.Item
//                 label="Full name"
//                 name="fullname"
//                 rules={[{ required: true, message: "Please input your name!" }]}
//               >
//                 <Input placeholder="Full name..." />
//               </Form.Item>
//               <Form.Item
//                 label="Email"
//                 name="email"
//                 rules={[
//                   { required: true, message: "Please input your email!" },
//                 ]}
//               >
//                 <Input placeholder="Email..." />
//               </Form.Item>
//               <Form.Item
//                 label="Phone"
//                 name="phone"
//                 rules={[
//                   {
//                     required: true,
//                     message: "Please input your phone!",
//                   },
//                 ]}
//               >
//                 <Input placeholder="Phone..." />
//               </Form.Item>
//
//               <Form.Item
//                 label="Password"
//                 name="password"
//                 rules={[
//                   { required: true, message: "Please input your password!" },
//                 ]}
//               >
//                 <Input.Password />
//               </Form.Item>
//               <Form.Item
//                 label="Confirm password"
//                 name="confirm_password"
//                 rules={[
//                   {
//                     required: true,
//                     message: "Please input your confirm password!",
//                   },
//                 ]}
//               >
//                 <Input.Password />
//               </Form.Item>
//
//               <Form.Item
//                 label="Company/Team/Store"
//                 name="team"
//                 rules={[{ required: true, message: "Please input your team" }]}
//               >
//                 <Input placeholder="Conpany of team..." />
//               </Form.Item>
//
//               <Form.Item label="Promotion Code" name="referrer">
//                 <Input placeholder="Promotion code..." />
//               </Form.Item>
//
//               <Form.Item>
//                 <Button
//                   className="w-right vcs-button w-200"
//                   type="primary"
//                   htmlType="submit"
//                   loading={loading}
//                 >
//                   Resiter
//                 </Button>
//                 <Link to="/signin" className="ml-4">
//                   Login
//                 </Link>
//               </Form.Item>
//             </Form>
//           </div>
//         </div>
//       </Col>
//       <Col xs={0} sm={12} md={12} lg={14}>
//         <div className="page_info">
//           <img className="page_img" alt="" src={wordwideBg} />
//           <div className="container">
//             <h1>Vận chuyển hàng e-commerce toàn cầu</h1>
//             <p>
//               VCS cung cấp dịch vụ vận chuyển e-commerce từ Việt Nam, Trung Quốc
//               tới tất cả các nước trên thế giới dù chỉ là một đơn hàng. Hàng hoá
//               có thể đi thẳng từ nhà sản xuất tới tận tay người tiêu dùng.
//             </p>
//           </div>
//         </div>
//       </Col>
//     </Row>
//   )
// }
//
// export default SignUp
