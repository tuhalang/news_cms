import React, {useState} from "react"
import {Checkbox, Form, Input, Button, Row, Col, message} from "antd"

// import { useDispatch } from "react-redux"
import {useHistory, Link} from "react-router-dom"
// import { setUserInfo } from "redux/actions"
// import getFactory from "api"
import "./index.scss"
import wordwideBg from "../../assets/images/wordwide.svg"
import {login} from "../../redux/actions";
import {useDispatch} from "react-redux";

// const apiUser = getFactory("user")
const Login = () => {
    // const history = useHistory()
    const dispatch = useDispatch()
    // const onChangeUserInfo = (value) => dispatch(setUserInfo(value))
    const [loading, setLoading] = useState(false)
    const r = localStorage.getItem("r")
    const _name = localStorage.getItem("n")

    function handleSubmit(values) {
        dispatch(login({...values}))
        // setLoading(true)
        // const res = await apiUser.login({ ...values, namespace: "vcs" })
        // const {
        //   type,
        //   token,
        //   fullname,
        //   avatar,
        //   merchant_type,
        //   refreshToken,
        //   agree_policy,
        // } = res
        // if (type === "Error") {
        //   message.error("Login failed")
        // } else {
        //   localStorage.setItem("accessToken", token)
        //   localStorage.setItem("refreshToken", refreshToken)
        //   localStorage.setItem("userName", fullname)
        //   localStorage.setItem("avatar", avatar)
        //   localStorage.setItem("merchant_type", merchant_type)
        //   localStorage.setItem("agree_policy", agree_policy || false)
        //   delete res.token
        //   onChangeUserInfo(res)
        //   setLoading(false)
        //   clearRegisterData()
        //   if (merchant_type === "customer_ship") {
        //     history.push("/shipping-orders")
        //   } else {
        //     message.error("Not authorized")
        //     localStorage.clear()
        //     history.push("/signin")
        //   }
        // }
    }

    function clearRegisterData() {
        localStorage.setItem("r", "")
    }

    function setUser(key) {
        let values = {}
        if (key === "logistics") {
            values = {
                email: "test@gmail.com",
                password: "123456",
                team: "Test customer ship",
            }
        }
        handleSubmit(values)
    }

    function onFinishFailed(params) {
    }

    return (
        <Row className="page-container vcs-page">
            <Col xs={24} sm={12} md={12} lg={10}>
                <div className="logo">
                    <div className="logo-vcs"/>
                    <div className="logo-text"> EXPRESS</div>
                </div>
                <div className="page_login">
                    <div className="login_form is-floating-label">
                        <div className="text4xl f-500 mb-4">Đăng nhập</div>
                        {r === "s" ? (
                            <p> Hello {_name}, hãy đăng nhập để sử dụng hệ thống </p>
                        ) : (
                            <p></p>
                        )}
                        <Form
                            layout="vertical"
                            name="basic"
                            initialValues={{remember: true}}
                            onFinish={handleSubmit}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[{required: true, message: "Vui lòng nhập email !"}]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Mật khẩu"
                                name="password"
                                rules={[
                                    {required: true, message: "Vui lòng nhập mật khẩu !"},
                                ]}
                            >
                                <Input.Password/>
                            </Form.Item>
                            <Form.Item>
                                <Form.Item name="remember" valuePropName="checked" noStyle>
                                    <Checkbox>Ghi nhớ </Checkbox>
                                </Form.Item>

                                <Link className="login-form-forgot" to="/forgot-password">
                                    Quên mật khẩu
                                </Link>
                            </Form.Item>
                            <Form.Item>
                                <Button
                                    className="w-right vcs-button w-200"
                                    type="primary"
                                    htmlType="submit"
                                    loading={loading}
                                >
                                    Đăng nhập
                                </Button>
                                &nbsp; Hoặc
                                <Link to="/signup"> đăng ký </Link>
                            </Form.Item>
                            {process.env.NODE_ENV === "development" && (
                                <Row>
                                    <Col>
                                        <Button type="text" onClick={() => setUser("logistics")}>
                                            Login as Customer VCS
                                        </Button>
                                    </Col>
                                </Row>
                            )}
                        </Form>
                    </div>
                </div>
            </Col>
            <Col xs={0} sm={12} md={12} lg={14}>
                <div className="page_info">
                    <img className="page_img" alt="" src={wordwideBg}/>
                    <div className="container">
                        <h1>Vận chuyển hàng e-commerce toàn cầu</h1>
                        <p>
                            VCS cung cấp dịch vụ vận chuyển e-commerce từ Việt Nam, Trung Quốc
                            tới tất cả các nước trên thế giới dù chỉ là một đơn hàng. Hàng hoá
                            có thể đi thẳng từ nhà sản xuất tới tận tay người tiêu dùng.
                        </p>
                    </div>
                </div>
            </Col>
        </Row>
    )
}

export default Login
