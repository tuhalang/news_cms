import React from "react";

import './editor.css'
import {Modal} from "antd";
import {Editor} from "@tinymce/tinymce-react";

const plugins = "print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons"
const toolbar = "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl"
const menubar = "file edit view insert format tools table help"
const imagetools_cors_hosts = ['picsum.photos']

//example: https://codepen.io/tinymce/pen/dyGPYqw

function TinyEditor({visible, onClose}) {

    const handleEditorChange = (e) => {
        console.log(
            'Content was updated:',
            e.target.getContent()
        );
    }

    return (
        <>
            <Modal
                visible={visible}
                onCancel={onClose}
                width={1000}
                className="modal-edit"
                bodyStyle={{
                    borderRadius: 15,
                }}
            >
                <div className="editor">
                    <Editor
                        apiKey="0mexo29p3o6n23gbxx4ud6vpoeau5wm3hdxe7l6y0w4mivmh"
                        init={{
                            height: 500,
                            menubar: menubar,
                            plugins: [plugins],
                            toolbar: toolbar
                        }}
                        onChange={handleEditorChange}
                    />
                </div>
            </Modal>
        </>
    )
}

export default TinyEditor
